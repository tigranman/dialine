$(document).ready(function() {
  function ipLookUp () {
  $.ajax('http://ip-api.com/json?lang=ru')
  .then(
      function success(response) {
          if(response.country != 'Russia') {
            $('.changable-region').html(response.regionName)
          } else {
            $('.changable-region').html('Страны СНГ');
          }
      },

      function fail(data, status) {
          console.log('Request failed.  Returned status of',
                      status);
      }
  );
}
  ipLookUp();
  // GETTINGS DATE
  var someDate = new Date();
  var numberOfDaysToAdd = 2;
  someDate.setDate(someDate.getDate() + numberOfDaysToAdd);

  var dd = someDate.getDate();
  var mm = someDate.getMonth() + 1;
  var y = someDate.getFullYear();

  var someFormattedDate = dd + '/'+ mm + '/'+ y;
  $('.x_price_previous').html(someFormattedDate);
  // GETTINGS DATE END
  // ITEM COUNTDOWN
  setTimeout(start, 500);

  var i = 60;
  // var num = document.getElementById('number');

  function start() {
    setInterval(increase, 10000);
  }

  function increase() {
      if (i > 7) {
        i--;
        $('.lastpack').html(i);
      }
  }
  // ITEM COUNTDOWN END

});
// POPUP
$('html').mouseleave('click', function () {
        $('.popup-box').addClass('open');
        $('.popup-box').removeClass('close');
    });
// POPUP END
// URL CALLBACK
$(window).load(function(){
  if(window.location.href.indexOf("callback=1") > -1) {
     alert("Есть параметр callback, со значением: 1");
  }
});
// URL CALLBACK END
